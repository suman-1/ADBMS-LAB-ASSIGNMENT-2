# `Lab 1`

**1.**  __Determine the structure of DEPARTMENTS table and contents.__


    DESCRIBE DEPARTMENTS
    SELECT * FROM DEPARTMENTS;
    

**2.**  __Determine the structure of Employees table.__
    
    Describe Employees;
    

    
**3.**  __The HR department wants a query to display the last name,job code, hire date and employee number for each employee, 
with the employee number appearing first. Provide an alias STARTDATE for the HIRE_DATE column.
Save your SQL statement to file named lab_01_03.sql so that you can dispatch this file to the HR department.__

    SELECT 
    EMPLOYEE_ID,LAST_NAME , JOB_ID , HIRE_DATE AS STARTDATE
    FROM EMPLOYEES;
        
        
**4.**  __The HR department needs a query to display all unique job codes (job_id) 
from the EMPLOYEES table.__

        SELECT
        DISTINCT(JOB_ID)
        FROM EMPLOYEES;
        
        

**5.**   __The HR department wants more descriptive column heading for its 
report on employees. Copy the statement from lab_01_03.sql.
Name the column heading EMP #, Employee, Job and Hire Date, 
respectively. Then run your query again.__

    SELECT 
    EMPLOYEE_ID  "EMP #",JOB_ID "JOB" , HIRE_DATE "HIRE DATE"
    FROM EMPLOYEES;
        
        
        
**6.**  __The HR department has requested a report of all employees and 
their job IDs. Display the last name concatenated with the job ID 
(Separated by a comma and space) and name the column Employee and Title.__

    SELECT
    LAST_NAME ||', '||JOB_ID "Employee and Title"
    FROM EMPLOYEES;
        


# `Lab 2`
## The HR department needs your assistance with creating some queries

**1.**  __Because of  budget  issues,  the  HR  department  needs  a  report  that 
displays  the  last  name  and  salary  employees  earning  more  than 
$12,000.  Save  your  SQL  statement  named  lab_02_01.sql.  Run 
your query.__

    SELECT 
    LAST_NAME , SALARY 
    FROM EMPLOYEES 
    WHERE SALARY >= 12000;
        
**2.**  __Write  a  query  that  displays  the  last  name  and  department  number 
for employee number 176.__
    
    SELECT 
    LAST_NAME , DEPARTMENT_ID "DEPARTMENT NUMBER"
    FROM EMPLOYEES
    WHERE EMPLOYEE_ID =176;
        
**3.**  __Write  a  query  that  displays  the  last  name  and  salary  for  all 
employees whose salary is not in the $5,000 -$12,000 range.__

    
    SELECT  LAST_NAME , SALARY 
    FROM    EMPLOYEES 
    WHERE   SALARY<5000
    OR  SALARY>12000;
    
**4.**  __Run a query to display the last name, job ID, and start date for the 
employees  whose  last  names  are  Matos  and  Taylor.  Order  the query
in ascending order by start date.__

    SELECT  LAST_NAME , JOB_ID, HIRE_DATE
    FROM    EMPLOYEES
    WHERE   LAST_NAME ='Taylor' OR LAST_NAME='Matos'
    ORDER BY    HIRE_DATE;
    
`Using IN for same Question`

    SELECT  LAST_NAME , JOB_ID , HIRE_DATE 
    FROM    EMPLOYEES
    WHERE   LAST_NAME IN ('Taylor','Matos')
    ORDER BY  HIRE_DATE;
    
**5.**  __Display the last name  and department number of all employees  in 
departments 20 or 50 in ascending alphabetical order by name.__

    SELECT  LAST_NAME , DEPARTMENT_ID 
    FROM    EMPLOYEES
    WHERE   DEPARTMENT_ID IN (20,50)
    ORDER BY    LAST_NAME;
    
**6.**  __Modify lab_02_01.sql to list the last name and salary of employees who earn between $5,000 and 
$12,000 and are in department 20 or 50. Label the columns Employee and Monthly salary, respectively. 
Resave lab_02_01.sql as lab_02_02.sql.Run lab_02_02.sql.__
    
    SELECT  LAST_NAME , SALARY 
    FROM    EMPLOYEES
    WHERE   SALARY BETWEEN 5000 AND 12000
    AND     DEPARTMENT_ID IN (20,50);
        
    
**7.**  __The HR department needs a report that displays the last name and hire date for all employees who 
were hired in 2003.__

    SELECT   LAST_NAME , HIRE_DATE 
    FROM     EMPLOYEES
    WHERE    HIRE_DATE LIKE '%03';

`another way`
    SELECT LAST_NAME , HIRE_DATE
    FROM EMPLOYEES
    WHERE EXTRACT(YEAR FROM HIRE_DATE)='2003';
        

**8.**  __Display  the  last  name  and  job  title  of  all  employees  who  do  not 
have a manager.__

    SELECT  LAST_NAME , JOB_ID 
    FROM    EMPLOYEES
    WHERE   MANAGER_ID IS NULL;
    
`Another way in Sql Server`

    SELECT LAST_NAME , JOB_ID
    FROM EMPLOYEES
    WHERE ISNULL(MANAGER_ID,'')=''
    

    

**9.**  __Display  the  last  name,  salary  and  commission  for  all  employees who earn commissions. 
Sort data in descending order of salary and commissions.__

    SELECT      LAST_NAME , SALARY , COMMISSION_PCT
    FROM        EMPLOYEES
    WHERE       COMMISSION_PCT IS NOT NULL
    ORDER BY    COMMISSION_PCT DESC , SALARY DESC;
        
    
**10.** __Members of the HR department want to have more flexibility with 
the  queries  that  you  are  writing.  They  would  like  a  report  that 
displays the last name and salary of employees who earn more than 
amount that the user specifies after a prompt.__
    
    SELECT  LAST_NAME ,SALARY
    FROM EMPLOYEES
    WHERE SALARY >&SAL_AMOUNT;

    
**11.** __The  HR  department  wants  to run  reports  based  on  a  manager. 
Create query that prompts the user for a manager ID and generates 
the   employee   ID,   last   name,   salary   and   department   for   that 
manager's employees. The HR department wants the ability to sort the  report  on  a  selected  column.
You  can  test  the  data  with  the following values:__
    * Manager ID=103, sorted by employee last name
    * Manager ID=201, sorted by salary
    * Manager ID=124, sorted by employee ID
    

        
        SELECT EMPLOYEE_ID, LAST_NAME , SALARY, DEPARTMENT_ID
        FROM EMPLOYEES
        WHERE MANAGER_ID = &ENTER_MANAGER_NUMBER 
        ORDER BY &ORDER_COL;
  
    
**12.** __Display  all  employee  last  names  in  which  the  third  letter  of  the 
name is a.__

    SELECT  LAST_NAME 
    FROM EMPLOYEES 
    WHERE LAST_NAME LIKE '__a%';
    

**13.** __Display the last names of all employees who have both an a and e
in their last names.__

    SELECT LAST_NAME 
    FROM EMPLOYEES 
    WHERE LAST_NAME LIKE '%a%'
    AND LAST_NAME like'%e%';


**14.** __Display the last name, job and salary for all employees whose job 
is  either  that  of  a  sales  representative  (SA_REP)  or  a  stock  clerk 
(ST_CLERK), and whose salary is not equal to $2,500, $3,500, or $7,000.__


    SELECT LAST_NAME , JOB_ID, SALARY 
    FROM EMPLOYEES
    WHERE JOB_ID IN ('SA_REP', 'ST_CLERK')
    AND SALARY NOT IN (2500,3500,7000);
    
    
**15.** __Modify   lab_02_02.sql   to   display   the   last   name,   salary   and 
commission for all employees  whose commission amount is 20%. 
Resave lab_02_02.sql as lab_02_3.sql and run it.__

    SELECT  LAST_NAME , SALARY , COMMISSION_PCT
    FROM EMPLOYEES 
    WHERE COMMISSION_PCT=.20;
    
    
# `Lab 3`
## USING SINGLE ROW FUNCTION TO CUSTOMIZE OUTPUT

**1.**  __Write a query to display the current data. Label the column Date.__

    SELECT SYSDATE "DATE"
    FROM DUAL;
    
**2.** __The HR department needs a report to display the employee number, last name, salary, and
salary increased by 15.5% (expressed as a whole number) for each employee. 
Label the column New Salary. Place your SQL statement in a text file named lab_03_01.sql.__
    
    SELECT EMPLOYEE_ID, LAST_NAME, SALARY ,
    ROUND(SALARY* 1.155,2) "NEW SALARY"
    FROM EMPLOYEES;
    
`Difference between rounding NUMBER and floating-point number values. NUMBER values are rounded up (for positive values), 
whereas floating-point numbers are rounded toward the nearest even value.`
    
    SELECT EMPLOYEE_ID, LAST_NAME, SALARY ,
    ROUND(SALARY* 1.157F,3) "NEW SALARY"
    FROM EMPLOYEES;
    
`SQL ROUND() Syntax`

    SELECT ROUND(column_name,decimals) FROM table_name; 
    
**3** __Modify your lab_03_01.sql query to add a column that subtracts the old salary from the new salary. Label the column Increase. 
Save the contents of the file as lab_03_02.sql.__

        CREATE VIEW  newsalary as
        SELECT EMPLOYEE_ID, LAST_NAME,
        ROUND(SALARY* 1.155,2) "NEW SALARY"
        FROM EMPLOYEES;
    
    
        select newsalary."NEW SALARY",employees.salary,(newsalary."NEW SALARY"-employees.salary) as "Salary Increase"
        from newsalary,employees
        where newsalary.employee_id=employees.employee_id;
        
`Another way`

        SELECT  employee_id, last_name, salary,  
        ROUND(salary * 1.155, 0) "New Salary", 
        ROUND(salary * 1.155, 0) - salary "Increase"  
        FROM    employees;

`Syntax for VIEW`

        select v.col1, t.col2
        from VIEW v, TABLE t
        where v.col1 = t.col1
        
        
**4** __Write a query that displays the last name and the length of the last  
name for all employees whose name starts with letters J, A or M. 
Give each column an appropriate label. Sort the results by the 
employees' last names.__

        SELECT  INITCAP(last_name), LENGTH(last_name)
        FROM    employees
        WHERE   last_name LIKE 'J%'
        OR      last_name Like 'A%'
        OR      last_name Like 'M%'
        ORDER BY last_name;
        
`Passing value to the last_name promt`

        SELECT  INITCAP(last_name), LENGTH(last_name)
        FROM    employees
        WHERE   last_name LIKE '&start_letter%'
        ORDER BY last_name;
        
**5** __Write a query that displays the last name (with the first letter 
uppercase and all other letters lowercase) and the length of the last 
name for all employees whose name starts with the letter J, A or 
M. Give each column an appropriate label. Sort the results by the 
employees' last names.__

        SELECT  INITCAP(last_name) ,LENGTH(last_name)
        FROM    employees
        WHERE   last_name LIKE'J%'
        OR      last_name LIKE'A%'
        OR      last_name LIKE'M%'
        ORDER BY    last_name;

    
**6** __Create a query to display the last name and salary for all 
employees. Format the salary to be 15 characters long, left-padded 
with the "$" symbol. Label the column SALARY.__

    SELECT  last_name ,
    LPAD(salary,15,'$')"SALARY"
    from employees;

[LPAD](https://docs.oracle.com/cd/B19306_01/server.102/b14200/functions082.htm)

# `Lab 4`
## Using Conversion Functions and Conditional Expressions

**1.**  __Using *CASE* function, write a query that displays the grade of all employees based on the value of the column JOB_ID, using the 
following data:__

| **Job**        | **Grade**     |
|----------------|---------------|
|AD_PRES|A|
|ST_MAN|B|
|IT_PROG|C|
|SA_REP|D|
|ST_CLERK|E|
|None of above|0|

    SElECT job_id , 
        CASE    job_id 
                WHEN 'AD_PRES' THEN 'A'
                WHEN 'ST_MAN' THEN 'B'
                WHEN 'IT_PROG' THEN 'C'
                WHEN 'SA_REP' THEN 'D'
                WHEN 'ST_CLERK' THEN 'E'
                ELSE '0' END GRADE
        FROM EMPLOYEES;
                
                
`Syntax For CASE`

    CASE [ expression ]
    
       WHEN condition_1 THEN result_1
       WHEN condition_2 THEN result_2
       ...
       WHEN condition_n THEN result_n
    
       ELSE result
    
    END
    

**2.**__Rewrite the statement in the preceding exercise using the 
DECODE function.__
    
    SElECT job_id , 
    DECODE  (job_id,
            'AD_PRES', 'A',
            'ST_MAN' , 'B',
            'IT_PROG' , 'C',
            'SA_REP' , 'D',
            'ST_CLERK' , 'E',
             '0') GRADE
    FROM EMPLOYEES;
